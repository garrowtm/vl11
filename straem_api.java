import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class straem_api
{
    public static void main(String[] args)
    {
        String test = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sodales consectetur purus at faucibus. Donec mi quam, tempor vel ipsum non, faucibus suscipit massa. Morbi lacinia velit blandit tincidunt efficitur. Vestibulum eget metus imperdiet sapien laoreet faucibus. Nunc eget vehicula mauris, ac auctor lorem. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel odio nec mi tempor dignissim.";

        Scanner scan = new Scanner(System.in, StandardCharsets.UTF_8);
        String txt = scan.nextLine();
        String[] words = txt.replaceAll("[^A-Za-zА-Яа-я-\s]", "").split(" ");

        Stream.of(words).
                map(word -> word.toLowerCase()). // String::toLowerCase
                collect(Collectors.groupingBy(word -> word, Collectors.counting())).
                entrySet().
                stream().
                sorted((map1, map2) -> map2.getValue().compareTo(map1.getValue())). // Map.Entry.comparingByValue()
                sorted((map1, map2) -> map1.getValue().equals(map2.getValue()) ? map1.getKey().compareTo(map2.getKey()) : map2.getValue().compareTo(map1.getValue())).
                limit(10).
                map(map -> map.getKey()). // Map.Entry::getKey
                forEach(str -> System.out.println(str)); // System.out::println
    }
}